package com.sbgitdemo.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.sbgitdemo.model.User;


@RestController
public class UserController {
	

	@GetMapping(value = "/emp") // master
	public User getEmployee() {
		return new User(10, "Hello", "World");
	}

	@GetMapping(value = "/search/{id}") // developer
	public User getUserById(@PathVariable("id") int userId) {
		User user = null;
		if (userId == 10) {
			user = new User(10, "Hello", "World");
		}
		if (userId == 20) {
			user = new User(20, "Lunch", "Break");
		}
		return user;
	}
		@GetMapping(value = "/list") // developer
		public List<User> getUsers(@PathVariable("id") int userId) {
			{
				User user1 = new User(10, "Hello", "World");
				User user2 = new User(10, "Hello", "World");
				User user3 = new User(10, "Hello", "World");
				List<User> users = new ArrayList<>();
			        users.add(user1);
			        users.add(user2);
			        users.add(user3);
			        return users;
			       
			    }
			}
		
	}

