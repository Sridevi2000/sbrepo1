package com.sbgitdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SbgitdemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SbgitdemoApplication.class, args);
	}

}
